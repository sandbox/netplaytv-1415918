Shine
------------------------
Requires Drupal 6

Author: Henri Grech-Cini / Marcelo Vani
Copyright: NetplayTv

Overview:
--------
Creates a Memory File system to store assets. i.e. Aggregated CSS files. Shine can be used to distribute assets across a multiple server archictecture. 
Shine is powered by jsmin-php https://github.com/rgrove/jsmin-php/

Installation:
-------------
1. Place this module directory in your modules folder (this will
   usually be "sites/all/modules/").
2. Go to "administer -> build -> modules" and enable the module.

3. Either copy the right patched block.module file or you can
   apply one of the core patches if you like. If you patch, copy
   the patchfile to Drupal's include folder and run following command:
   
   patch -p0 < common.inc.patch
   
   To reverse the patch, simple run following command:
   
   patch -R -p0 < common.inc.patch

Configuration:
--------------
Go to "administer -> settings -> shine"
	Set the caching expiration

Go to "administer -> settings -> performance"
	Enable Optimize CSS files
	Enable Store Aggregated CSS in Memcache

	Enable Optimize JAVASCRIPT files
	Enable Store Aggregated JS in Memcache

Last updated:
------------
; $Id: README.txt,v 1.0 2011/10/05 16:35:58 mvani Exp $
