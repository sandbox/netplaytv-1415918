<?php 
// $Id: shine.module ,v 1.0 2010/10/04 16:38:44 mvani Exp $
/**
 * @file
 * Provides backend support for NetPlayTV Websites
 * 
 * @copyright   Copyright (C) 2010 NetPlayTV
 * @author      Henry Grech-Cini <henry.grechcini@netplaytv.com>
 * 				Marcelo Vani <marcelo.vani@netplaytv.com>
 * @date		04/10/2010
 *
 */

/*
 * Admin Settings
 */
function shine_admin_form() {
  $form = array();

  /*
   * Headers cache expiration
   * Supported file types: 
   *     CSS
   *     JS
   * 
   * Feel free to include additional settings for other file types
   */
  $form['headers'] = array(
    '#type' => 'fieldset',
    '#title' => t('<div class="title">File types and headers</div>'),
  );
    $form['headers']['shine_css_header_expire'] = array(
      '#type'          => 'textfield',
      '#title'         => t('CSS Header expiration'),
      '#size'          => 4,
      '#default_value' => variable_get('shine_css_header_expire', '300'),
      '#description'   => t('Specify the header expiration for the storage. Time in seconds. Default: 300'),
      );
    $form['headers']['shine_js_header_expire'] = array(
      '#type'          => 'textfield',
      '#title'         => t('JS Header expiration'),
      '#size'          => 4,
      '#default_value' => variable_get('shine_js_header_expire', '300'),
      '#description'   => t('Specify the header expiration for the storage. Time in seconds. Default: 300'),
    ); 
    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('<div class="title">General</div>'),
    );
      $form['general']['shine_use_boot'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Enable Bootstrap boost'),
        '#default_value' => variable_get('shine_use_boot', 0),
        '#description'   => t('When enabled, the module will load faster and use less system resources'),
        );  
      
  return system_settings_form($form);
}












