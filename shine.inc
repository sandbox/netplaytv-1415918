<?
$query_string = @$_REQUEST['q'];
if (substr(strtolower($query_string),0,3) == 'mem') {
  $conf['file_directory_temp'] = '/tmp';
  
  require_once './includes/common.inc';
  require_once './includes/file.inc';
  require_once './includes/module.inc';

  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

  require_once('./sites/all/modules/netplaytv/utils/utils.module');
  require_once('./sites/all/modules/netplaytv/shine/shine.module');
  
  shine_variable_init('shine_css');
  shine_variable_init('shine_js');
  shine_variable_init('shine_minify_js');
  shine_variable_init('shine_minify_plus_js');
  shine_variable_init('shine_css_header_expire');
  shine_variable_init('shine_js_header_expire');
  
  $file = substr($query_string, 4, strlen($query_string)-3);
  
  echo shine_output_file($file);
  
  exit;
}

/*
 * shine_variable_init
 * This function does the same job as drupal's variable_init 
 * but it does it for a specific variable instead of populating 
 * all variables in the database
 * This function is necessary when jumping the bootstraps
 */
function shine_variable_init($name) {
	global $conf;

	$result = db_query("SELECT * FROM variable WHERE name = '%s'", $name);
	
	if ($result!==false) {
		$row = db_fetch_object($result);
		if (isset($row->value)) {
			$conf[$name] = unserialize($row->value);
		}
	}
}
